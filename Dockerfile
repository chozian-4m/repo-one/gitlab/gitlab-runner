ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM $BASE_REGISTRY/$BASE_IMAGE:$BASE_TAG

ENV HOME /home/gitlab-runner

# Install security updates that might not be included in the base image.
# Without the latest security updates the image can't pass certification.
RUN dnf --disableplugin=subscription-manager -y update-minimal \
    --security --sec-severity=Important --sec-severity=Critical

RUN dnf --disableplugin=subscription-manager install -yb --nodocs \
    ca-certificates \
    openssl \
    procps \
    tzdata

COPY ./tini /usr/local/bin/tini
COPY ./gitlab-runner /usr/local/bin/gitlab-runner

COPY ./scripts/entrypoint.sh /entrypoint
COPY LICENSE /licenses/

RUN chmod +x /usr/local/bin/gitlab-runner && \
    chmod +x /usr/local/bin/tini && \
    chmod +x /entrypoint

# https://docs.openshift.com/container-platform/4.6/openshift_images/create-images.html#support-arbitrary-user-ids
RUN mkdir -p /etc/gitlab-runner/certs $HOME /secrets && \
    chgrp -R 0 /etc/gitlab-runner && \
    chmod -R g=u /etc/gitlab-runner && \
    chgrp -R 0 $HOME && \
    chmod -R g=u $HOME && \
    chgrp -R 0 /secrets && \
    chmod -R g=u /secrets

USER 1001

STOPSIGNAL SIGQUIT
ENTRYPOINT ["tini", "--", "/entrypoint"]
CMD ["run", "--working-directory=/"]
