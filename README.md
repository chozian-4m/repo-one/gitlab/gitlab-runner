# gitlab-runner

# Description

GitLab Runner is an open source project that is used for running jobs with GitLab. It is able to send the results of the job back to GitLab and is utilized with GitLab CI/CD.

Please refer to the official documentation here for GitLab Runner:

https://docs.gitlab.com/runner/

The above documentation contains information on GitLab runner features, configuration, and troubleshooting, for example, but includes much more information as well.

## Obtaining the GitLab Runner Image

There are two ways to obtain the GitLab Runner image.

1. Navigate to the Iron Bank website and the GitLab Runner project. Download the tar file of the image and run `docker load -i <img_name>.tar` to load the image.
2. If you have access to the IronBank Harbor registry, look in the `/ironbank` project for the `gitlab/gitlab-runner/gitlab-runner:v13.11.0` image and pull it from the registry.

## Configuration

You will need to register the container after it has been created.

https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1285#note_13221145

To do this, run `docker exec -it gitlab-runner gitlab-runner register` and enter the gitlab-ci coordinator URL so that a `config.toml` file can be created and the gitlab-runner can configure it. Otherwise, you will encounter problems with the `config.toml` file not existing.

### Notes

The user used for the container is `gitlab-runner`.
